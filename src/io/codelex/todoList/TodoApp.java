package io.codelex.todoList;


import java.util.Scanner;

public class TodoApp {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        boolean menu = true;


        System.out.println("1 - Add new task");
        System.out.println("2 - Show all tasks per day");
        System.out.println("3 - Make task complited");
        System.out.println("4 - Make task favorite");
        System.out.println("5 - Delete task");
        System.out.println("6 - Delete all tasks");
        System.out.println("7 - Exit");
        System.out.print("Enter your choice:");

        Integer userChoice = scanner.nextInt();
        TodoServices todoServices = new TodoServices();
        while (menu){
            switch (userChoice) {
                case 1:
                    todoServices.addNewTask();
                    break;
                case 2:
                    todoServices.showAllTaskPerDay();
                    break;
                case 3:
                    todoServices.makeTaskComplited();
                    break;
                case 4:
                    todoServices.makeTaskFavorite();
                    break;
                case 5:
                    todoServices.deleteTask();
                    break;
                case 6:
                    todoServices.deleteAllTasks();
                    break;
                case 7:
                    menu = false;
                    break;
            }
    }

    }


}
