package io.codelex.todoList;

import java.util.Scanner;

public class TodoServices {

    Scanner scanner = new Scanner(System.in).useDelimiter("\n");

    public void addNewTask() {
        System.out.print("Enter task: ");
        String task = scanner.next();
        System.out.print("Enter a date, YYYY-MM-DD, ");
        System.out.print("year: ");
        Integer year = scanner.nextInt();
        System.out.print("month: ");
        Integer month = scanner.nextInt();
        System.out.print("day: ");
        Integer day = scanner.nextInt();
        boolean taskFavoriteMark = false;
        boolean correctEnterMark = false;
        do {
            System.out.print("Task '" + task + "' is favorite(Y/N)? ");
            char choiseTaskFavorite = scanner.next().charAt(0);
            switch (choiseTaskFavorite) {
                case 'y':
                    taskFavoriteMark = true;
                    correctEnterMark = true;
                    break;
                case 'Y':
                    taskFavoriteMark = true;
                    correctEnterMark = true;
                    break;
                case 'n':
                    taskFavoriteMark = false;
                    correctEnterMark = true;
                    break;
                case 'N':
                    taskFavoriteMark = false;
                    correctEnterMark = true;
                    break;
            }
            if (correctEnterMark == false) {
                System.out.println("Input error!");
            }
        } while (correctEnterMark == false);

        Task newTask = new Task(task, year, month, day, taskFavoriteMark, false);
        TodoList.todoList.add(newTask);
    }


    public void showAllTaskPerDay() {
        System.out.print("Enter a date, YYYY-MM-DD ");
        System.out.print("year: ");
        Integer year = scanner.nextInt();
        System.out.print("month: ");
        Integer month = scanner.nextInt();
        System.out.print("day: ");
        Integer day = scanner.nextInt();
        for (Task task : TodoList.todoList) {
            if (year.equals(task.getYear()) && month.equals(task.getMonth()) && day.equals(task.getDay()) == true) {
                if (task.isCompletedTask() == true) {
                    System.out.println("Complited task: ");
                    System.out.println(task);
                }
            }
        }
        for (Task task : TodoList.todoList) {
            if (year.equals(task.getYear()) && month.equals(task.getMonth()) && day.equals(task.getDay()) == true) {
                if (task.isCompletedTask() == false) {
                    System.out.println("Uncomplited task: ");
                    System.out.println(task);
                }
            }
        }
    }

    public void makeTaskComplited() {
        System.out.print("Enter task: ");
        String complitedTask = scanner.next();
        for (Task task : TodoList.todoList) {
            if (complitedTask.equals(task.getTask())) {
                task.setCompletedTask(true);
            }
        }
    }

    public void makeTaskFavorite() {
        System.out.print("Enter task: ");
        String complitedTask = scanner.next();
        for (Task task : TodoList.todoList) {
            if (complitedTask.equals(task.getTask())) {
                task.setFavoriteTask(true);
            }
        }
    }

    public void deleteTask() {
        System.out.print("Enter task: ");
        String complitedTask = scanner.next();
        for (Task task : TodoList.todoList) {
            if (complitedTask.equals(task.getTask())) {
                TodoList.todoList.remove(task);
            }
        }
    }

    public void deleteAllTasks(){
        TodoList.todoList.clear();
    }


}