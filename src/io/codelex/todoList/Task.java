package io.codelex.todoList;

import java.util.Objects;

public class Task {

    String task;
    Integer year;
    Integer month;
    Integer day;
    boolean favoriteTask;
    boolean completedTask;

    public Task(String task, Integer year, Integer month, Integer day, boolean favoriteTask, boolean completedTask) {
        this.task = task;
        this.year = year;
        this.month = month;
        this.day = day;
        this.favoriteTask = favoriteTask;
        this.completedTask = completedTask;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public boolean isCompletedTask() {
        return completedTask;
    }

    public void setCompletedTask(boolean completedTask) {
        this.completedTask = completedTask;
    }

    public boolean isFavoriteTask() {
        return favoriteTask;
    }

    public void setFavoriteTask(boolean favoriteTask) {
        this.favoriteTask = favoriteTask;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task1 = (Task) o;
        return completedTask == task1.completedTask &&
                favoriteTask == task1.favoriteTask &&
                Objects.equals(year, task1.year) &&
                Objects.equals(month, task1.month) &&
                Objects.equals(day, task1.day) &&
                Objects.equals(task, task1.task);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, month, day, task, completedTask, favoriteTask);
    }


    @Override
    public String toString() {
        return "Task{" +
                "year=" + year +
                ", month=" + month +
                ", day=" + day +
                ", task='" + task + '\'' +
                ", favoriteTask=" + favoriteTask +
                '}';
    }
}
